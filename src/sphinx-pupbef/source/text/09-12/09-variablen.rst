
*********
Variablen
*********


Variablenaufbereitung
=====================

Bei vielen String- und Listen-Funktionen wurde auf das kosmetische Entfernen von Leerzeichen verzichtet. Dadurch verändert sich die Struktur der Liste nicht ungewollt.



Trimmen bei Variablenauswertung
-------------------------------

* Man kann statt "[name]" auch Leerzeichen einfügen, etwa " [ name ]".
* Wird dies so gemacht, so wird angenommen, daß die Leerzeichen vorne und	hinten beim Inhalt dieser Variablen nicht mitgeliefert (=entfernt) werden sollen.


*Beispiel*::

  ACTION eineAction
    SET Test1 "1 Punkt" ## Neuer Inhalt von Test1: "1 Punkt"
    EVAL Test1 = WITHOUTFIRSTCHAR [Test1] ## Neuer Inhalt von Test1: " Punkt"
    EVAL Test1 = WITHOUTFIRSTCHAR [Test1] ## Neuer Inhalt von Test1: "Punkt"
    >> Test1 ***[Test1]
    ## Ausgabe: "Test1 ***Punkt"
    SET Test2 "1 Punkt" ## Neuer Inhalt von Test2: "1 Punkt"
    EVAL Test2 = WITHOUTFIRSTCHAR [ Test2 ] ## Neuer Inhalt von Test2: " Punkt"
    EVAL Test2 = WITHOUTFIRSTCHAR [ Test2 ] ## Neuer Inhalt von Test2: "unkt"
    >> Test2 ***[Test2]
    ## Ausgabe: "Test2 ***unkt"
  END


.. note::

  Dies zeigt auch, dass erst die Variable getrimmt wird, und erst dann der Ausdruck ``WITHOUTFIRSTCHAR`` ausgeführt wird.


.. _variablenersetzung:

Variablenersetzung
==================

* Kommt in einem der Parameter irgendeines Befehls ein eckiges Klammernpaar vor, so wird vor Ausführung des Befehls der Variablenname zwischen den Klammern durch dessen Wert ersetzt.
* Eckige Klammern können verschachtelt werden.


*Beispiel*::

  # --------------------------------------------------------------- init
  ACTION init
    # --- Informationen einholen
    WHOIS STARTER
    SET _starter_ [WHO]
    # --- Namen für Nummern der Save-Variablen
    SET _SaveConfig_      10
    SET _SaveConfig_City_  1
    SET _SaveConfig_Home_  2
    # --- Save-Variable: Config
    IF EXISTS SAVE[_SaveConfig_]
      BEGIN
        EVAL _city_ = [_SaveConfig_City_] ELEMENTOF [SAVE[_SaveConfig_]]
        EVAL _home_ = [_SaveConfig_Home_] ELEMENTOF [SAVE[_SaveConfig_]]
        >> /tell [_starter_] Konfiguration: [SAVE[_SaveConfig_]]
        >> /tell [_starter_] Puppet ist konfiguriert für city=[_city_] und home=[_home_]
      END
    # ...
  END


Möchte man eckige Klammern nicht ersetzen lassen, so muss man sie durch einen Backslash (\\) vor der Klammer zu einer normalen Klammer machen. Einem Backslash muss man ebenfalls einen (weiteren) Backslash voranstellen.


*Beispiel*::

  ACTION eineAction
    SET y 1
    >> /tell Kugelschreiber y = 1 : [y]
    EVAL y = [y] + 2
    >> /tell Kugelschreiber y = \[y\] + 2 : [y]
  END

*Zugehörige Ausgabe*::

  kuli997: y = 1 : 1
  kuli997: y = [y] + 2 : 3




Variablendefinition
===================

.. index:: pair: CHARSET-Variable; Variable

``CHARSET``

  Zeichensatz des Puppetsource (Beispiel: ISO-8859-1).

.. index:: pair: __FILE-Variable; Variable

``__FILE``

  Name der Puppetsource (ohne Dateiendung).

.. index:: pair: __GAME-Variable; Variable

``__GAME<#M><S>``

  Rückgabe der Anzahl Spieltaler des angebenen Spiels `S` bei der ebenfalls angegebenen Anzahl an Mitspielern `#M` (Beispiel: ``[__GAME4CantStop] = 7``).

  Wird als ``#M`` eine ungültige Mitspieler-Anzahl übergeben (z.B. "0"), so wird	die Spiel-ID des Spiels (CantStop 3, Crosswise 115, ...) zurückgegeben.

.. index:: pair: __MAXPUBLIC-Variable; Variable

``__MAXPUBLIC``

  Anzahl der dem Puppet zur Verfügung stehenden PUBLIC-Variablen. Diese Anzahl kann durch SLC erhöht werden.

  *Default*: 100

.. index:: pair: __MAXSAVE-Variable; Variable

``__MAXSAVE``

  Anzahl der dem Puppet zur Verfügung stehenden SAVE-Variablen. Diese Anzahl kann durch SLC erhöht werden.

  *Default*: 100

.. index:: pair: __VERSION-Variable; Variable

``__VERSION``

  Versions-Nr. von ??? (Beispiel: "$Revision: 1.126 $").


:Quelle: BSW Puppet Forum >> GURU - Best Practices der Puppet-Programmierung - Antwort #18



.. index:: pair: WHO-Variable; Variable

Variable WHO
------------

* Stammt eine Ausgabe vom Server ist ``[WHO]`` mit ``--`` belegt.
* Stammt der Text von einem //-Befehl so steht ``//`` drin.
* Ansonsten ist der Name des Schreibers enthalten.



.. index:: pair: TYPE-Variable; Variable

.. _VariableTYPE:

Variable TYPE
-------------

Diese Variablen gibt die Art des Chats an:

* ``CHAT`` = Eingabe erfolgt im Haupt-Chat-Fenster
* ``TELL`` = Eingabe erfolgte in einem Tell-Fenster zum Puppet
* ``CTELL`` = Eingabe erfolgte im Stadt-Chat-Fenster
* ``GTELL`` = Eingabe erfolgte in einem Channel-Fenster



.. index:: pair: _variableROOMNAME-Variable; Variable

.. _VariableROOM:

Variable ROOM
-------------

Diese Variable gibt den Aufenthaltsort des Sprechers - in Abhängigkeit der Variablen ``TYPE`` - an:

* Type ``CHAT``: Name des Raums, in dem sich der Sprecher befindet
* Type ``TELL``: Name des Raums, in dem sich der Sprecher befindet
* Type ``CTELL``: Name der Stadt
* Type ``GTELL``: Name des Kanals



.. index:: pair: ROOMNAME-Variable; Variable

.. _VariableROOMNAME:

Variable ROOMNAME
------------------

Diese Variable gibt eine Angabe zum Aufenthaltsort des Sprechers - in Abhängigkeit der Variablen ``TYPE`` - an:

* Type ``CHAT``: Name der Stadt
* Type ``TELL``: Name der Stadt
* Type ``CTELL``: Name des Kanals (#<Stadtname>)
* Type ``GTELL``: #Name des Kanals (#<Channelname>)
