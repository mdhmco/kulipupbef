
Ausdrücke für Boolsche Werte
============================


.. index:: pair: OR-Ausdruck; Ausdruck
.. index:: pair: AND-Ausdruck; Ausdruck

OR / AND
--------

::

  <boolean> OR <boolean>

Liefert ``TRUE``, falls einer der beiden Wahrheitswerte ``TRUE`` ist, ansonsten ``FALSE``.


::

  <boolean> AND <boolean>

Liefert ``TRUE``, falls beide Wahrheitswerte ``TRUE`` sind, ansonsten ``FALSE``.



.. index:: pair: NOT-Ausdruck; Ausdruck

NOT
---

::

  NOT <boolean>

Liefert ``TRUE``, wenn der Wahrheitswert ``FALSE`` ist, ansonsten ``FALSE``.
