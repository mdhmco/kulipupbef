
.. _auswertung:

Auswertung
==========

::

  ( <ausdruck> )

* Liefert <ausdruck> zurück.



.. _enggeschrFormeln:

Enggeschriebene Formeln
-----------------------

Bei den Operatoren ``= == != < <= > >= + - * / % ( )`` darf man auch eng schreiben, weil implizit Leerzeichen eingefügt werden (Operator-Tokens).


*Beispiel*::

  >> Wieviel ist 6 mal 9? [=6*9]
  # -- statt --
  >> Wieviel ist 6 mal 9? [=6 * 9]


Dieses Feature hat leider nicht nur Vorteile: Wenn diese Operatoren in Namen oder Schlüsselwörtern vorkommen, sollen sie natürlich nicht in dieser Weise interpretiert werden. Hier hilft die Benutzung von Gänsefüßchen "": 

::

  # - richtig -
  >> WHEN KEYWORD "!weg!" DO goHome
  # - falsch -
  >> WHEN KEYWORD !weg! DO goHome



Auswertung von verknüpften Ausdrücken (short circuit)
-----------------------------------------------------

``AND`` und ``OR`` wurde auf "short circuit" umgestellt, d.h. wenn der Parameter vor ``AND`` / ``OR`` bereits festlegt, wie das Ergebnis ist, wird der Parameter dahinter gar nicht mehr ausgewertet. 

* Bei ``AND`` ist dies der Fall, wenn der linke Parameter ``FALSE`` ist.
* Bei ``OR`` ist der Fall, wenn der linke Parameter ``TRUE`` ist.

:Quelle: BSW Puppet Forum >> Bereichsfunktionen TO, DOWNTO, STEP / Alternative Benutzung von FOR-Schleifen - Antwort #2



Dynamische Ausdruck-Auswertung
------------------------------

Der Ausdruck zwischen ``[=`` und der zugehörigen Klammer ``]`` wird geparsed und ausgewertet. Es gibt für ``[=...]`` nur zwei sinnvolle Anwendungen:

* Wenn für einen Ausgabe noch ein wenig herumgerechnet werden soll.
* Wenn vom Benutzer eine dynamische Formel eingegeben wird.


.. tip::

  In den allermeisten Fällen hätten einfache Klammern ``()`` denselben Zweck erfüllt.


*Beispiel*::

  # Beispiel aus der alten Version der Dokumentation (ca. 2005)
  # - unnötig -
  GETINFO "[=LASTOF [Liste]]"
  # - ausreichend -
  EVAL name = LASTOF [Liste]
  GETINFO [name]
  #
  # Beispiel: Berechnung bei der Ausgabe
  >> Dies entspricht einem Anteil von [=100 * [anz] / [gesamt]] Prozent!
  #
  # Beispiel: Gleicher Effekt durch die Verwendung einfacher Klammern
  # - falsch -
  IF [=[n] ELEMENTOF [Userlist]] INLIST [Winnerlist]
  # - richtig -
  IF ( [n] ELEMENTOF [Userlist] ) INLIST [Winnerlist]


Besteht der gesamte Ausdruck nur aus einer Variablenersetzung, so wird erst diese ausgeführt.

*Beispiel*::

  SET Ausdruck "5 * 7"
  # [=[Ausdruck]] liefert nun den Wert "35"


:Quelle: BSW Puppet Forum >> Kann ich mit listen nicht umgehen ??? - Antwort #7 und #11]
