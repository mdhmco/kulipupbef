
.. _typumwandlung:

Ausdrücke für Typumwandlung
===========================


BOOLEAN
-------

::

  BOOLEAN <ausdruck>

* Wandelt <ausdruck> in ein ``TRUE`` oder ``FALSE``.
* Ein Zahlenwert von "0" wird in ein ``FALSE``, jeder andere Zahlwert in ein ``TRUE`` umgewandelt. 

  
*Beispiel*::

  ACTION eineAction
  # Ergebnis FALSE
    SET expr "BOOLEAN 0"
    >> /tell Kugelschreiber [expr] = [=[expr]]
  # Ergebnis TRUE
    SET expr "BOOLEAN 1"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "BOOLEAN 2"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "BOOLEAN 3"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "BOOLEAN ( 27 / 6 )"
    >> /tell Kugelschreiber [expr] = [=[expr]]
  END



INTEGER
-------

::

  INTEGER <ausdruck>

* Wandelt <ausdruck> in eine Integer-Zahl um. 


*Beispiel*::

  ACTION eineAction
  # Ergebnis 4
    SET expr "27 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "INTEGER 27 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "INTEGER 27.0 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
  END



FLOAT
-----

::

  FLOAT <ausdruck>

* Wandelt <ausdruck> in eine Float-Zahl um.


*Beispiel*::

  ACTION eineAction
  # Ergebnis 4.5
    SET expr "27.0 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "FLOAT 27 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "FLOAT 27.0 / 6"
    >> /tell Kugelschreiber [expr] = [=[expr]]
  # Ergebnis -3.3333333333333335
    SET expr "-10.0 / 3"
    >> /tell Kugelschreiber [expr] = [=[expr]]
  # Ergebnisse in Floatdarstellung
    SET expr "1 / 1000000.0" # 1.0E-6
    >> /tell Kugelschreiber [expr] = [=[expr]]
    SET expr "111 / 1000000.0" 1.11E-4
    >> /tell Kugelschreiber [expr] = [=[expr]]
  END


.. note::

  Das mittlere Beispiel zeigt, dass die Beschränkung auf eine bestimmte Anzahl von Nachkommastellen wünschenwert wäre. Hierzu will sich SLC noch etwas einfallen lassen... 
