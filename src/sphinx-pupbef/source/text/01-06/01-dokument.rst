

.. ####################################################

.. .. index:: XXXX

.. .. index::
      single: Configuration storage
      single: phpMyAdmin configuration storage
      single: pmadb

.. .. index:: pair: @-Befehle; Befehl



.. .. seealso::

..    Kapitel :ref:`@-Befehle <addBefehle>`

.. Directives added by Sphinx are described in :doc:`02-aufbau`.

.. ####################################################



************************
Allgemeines zum Dokument
************************

Die Motivation zur Erstellung dieses Dokuments war im Februar 2006 das Bestreben, auf eine aktuelle Befehlsliste der Puppet-Programmierung in der Brettspielwelt (=BSW) [#fnBSW]_ Zugriff zu haben. Seitdem sind einige Jahre vergangen und die Häufigkeit der Aktualisierung hat kontinuierlich nachgelassen.

Die Bedeutung von Puppets in der BSW hat aber auch seit Einführung der mobilen Version und damit der "Apps" stark nachgelassen. Neuerungen gab es schon lange nicht mehr.

Vielleicht ist diese Dokumentation daher genauso wie die Puppets ein Relikt aus vergangenen Zeiten...

**Neuerung in 2019/2020**:

Mit der DSGVO sollte das Thema "Apps" zum Erliegen gekommen sein. Damit werden Puppets für (user-spezifische) "Events" wieder aktuell.

.. note::

  Es handelt sich um *KEIN* offizielles Dokument der BSW!



Randbedingungen
===============

Folgende Randbedingungen gelten für das Dokument:

- Das Dokument wird aktuell vom BSW-User "Kugelschreiber" erstellt.
- Im Dokument sind die Inhalte aus der offiziellen BSW Puppet-Hilfe sowie die sporadischen Informationen innerhalb des BSW Puppet-Forums   enthalten.



Dokumentation in verschiedenen Formaten
=======================================

Dies ist ein weiterer, aber auch letzter Anlauf, die Dokumentation in einer Form bereitzustellen, die mit vertretbarem Aufwand eine gewisse Anzahl an Ausgabenformaten unterstützt.

Mittlerweilen basiert diese Dokumentation auf Sphinx [#fnShinx]_.



Historie des Dokuments
======================


Dokumentation auf Basis von Sphinx

  * V1.41.0-x (04/2020)

    * Geringfügige Überarbeitungen
    * Korrektur der Findings von VS Code Extension "reSTructuredText" (mit Linter "doc8")
    * Bereitstellung in einem öffentlichen Repository
    * (+) SQRT

  * V1.40.x (04/2020)

    * Geringfügige Überarbeitungen
    * Vorbereitungen für die Bereitstellung in einem öffentlichen Repository

  * V1.30.x (09/2018 - 01/2019)

    * Geringfügige Überarbeitungen
    * Festlegung der Lizenzbedingungen
    * Umstellung auf **Sphinx**

Dokumentation auf Basis von Pandoc

  * V1.20 (07/2018)

    * Rück-Umstellung auf **Pandoc**

Dokumentation auf Basis von DokuWiki

  * V1.11 (09/2017)

    * Ergänzung zu “WHEN TIME”

  * V1.10 (01/2017)

    * (+) Spielersetzung “EinfachGenial” durch “Imhothep”.

  * V1.01 (12/2016) - V1.03 (12/2016)

    * Geringfügige Überarbeitungen
    * Umstellung auf **DokuWiki**

Dokumentation auf Basis div. Tools

  * Dieser Teil der Historie wurde gelöscht!
  * Ausnahme der folgende Eintrag:

    * Erste Version des Dokuments in **2006** V0.01 (18.02.2006)


.. [#fnBSW] http://www.brettspielwelt.de
.. [#fnShinx] http://www.sphinx-doc.org
