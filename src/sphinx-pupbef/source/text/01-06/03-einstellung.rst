
.. index:: Puppeteinstellung

.. _puppeteinstellungen:

*******************
Puppeteinstellungen
*******************

Es gibt eine Reihe von Einstellungen, die schon vor dem Ausführen der ersten Action bekannt sein müssen. Diese werden direkt hinter dem ``PUPPET`` Befehl angegeben:

::

  LOGIN: <string>
  LOGOUT: <string>
  APPEAR: <string>
  DISAPPEAR: <string>
  INFO: <string>
  NATION: bsw/<lang>
  CITYCHAT: YES/NO
  SAVE: <phrase>
  OWN: YES/NO
  NEWOWNER: YES/NO
  TWINDOW: YES/NO
  CASESENSITIV: YES/NO
  OVERFLOW: ERROR/HARAKIRI/IGNORE
  ZEROINTVARS: YES/NO
  DEBUG: <liste>
  @<command>: <action>
  CHARSET: <string>



Einstellungen für Puppettexte
=============================

.. index:: pair: LOGIN-Option; Puppeteinstellung

LOGIN
  Diese Einstellung legt den Text fest, der bei der Anmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.

  *Default*: " materialisiert sich aus milliarden winziger Teilchen."

.. index:: pair: LOGOUT-Option; Puppeteinstellung

LOGOUT
  Diese Einstellung legt den Text fest, der bei der Abmeldung ausgegeben wird. Leerzeichen am Anfang nicht vergessen.

  *Default*: " zerfaellt zu Staub."

.. index:: pair: APPEAR-Option; Puppeteinstellung

APPEAR
  Diese Einstellung legt den Text fest, der beim Betreten eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.

  *Default*: " verdichtet sich aus einer Wolke glitzernder Partikel."

.. index:: pair: DISAPPEAR-Option; Puppeteinstellung

DISAPPEAR
  Diese Einstellung legt den Text fest, der beim Verlassen eines Raumes ausgegeben wird. Leerzeichen am Anfang nicht vergessen.

  *Default*: " zerspringt in millionen funkelnder Partikel."

.. index:: pair: INFO-Option; Puppeteinstellung

INFO
  Es wird die Beschreibung des Puppets festgelegt, die beim `/info`-Kommando angezeigt wird.

  *Default*: "Ein CommandPuppet."




Einstellungen für Puppetcode
============================

.. index:: pair: NATION-Option; Puppeteinstellung

NATION
  Entspricht in der BrettspielWelt.prop in etwa ``NATION = <lang>`` oder einem frühzeitigen ``>> /language <lang>`` z.B. in der ACTION "start".

  *Default*: bsw

  .. tip::

    Die Sprache "bsw" ist zu empfehlen, da diese nicht durch unbedarfte Übersetzer kaputt gemacht werden kann.

.. index:: pair: CITYCHAT-Option; Puppeteinstellung

CITYCHAT
  Legt fest, ob das Puppet den Citychat des Starters benutzt. Bei Autostart-Puppets gilt derjenige als Starter, der das Puppet letztmalig manuell gestartet hat.

  - ``CITYCHAT:YES`` bewirkt automatisch ``NEWOWNER:NO``.
  - Wird ein Puppet von einem anderen Puppet gestartet, so kann es Probleme mit dem Stadt-Chat geben

  *Default*: NO


.. index:: pair: SAVE-Option; Puppeteinstellung

SAVE
  Der SAVE-Befehl macht die SAVE-Variablen persistent, d.h. wenn das Puppet wieder geladen wird, sind diese Variablen mit den gespeicherten Werte belegt.

  - Die Angabe "phrase" sollte weltweit eindeutig sein (Sonderzeichen werden automatisch durch "_" ersetzt).
  - "*" in "phrase" wird durch den Namen ersetzt, mit dem das Puppet in der BSW gestartet wird (dies erhöht die Chance auf Eindeutigkeit).
  - Bei gleichem Namen wird dieselbe Datenbasis benutzt. Dies kann in seltenen Fällen Sinn machen (allerdings sollten zwei Puppets mit denselben Daten	nicht gleichzeitig gestartet sein).

  Wird ``SAVE:`` nicht angegeben wird, dann werden keine SAVE-Variablen gespeichert.

  *Default*: "---"




Einstellungen für Puppetowning
==============================

.. index:: pair: OWN-Option; Puppeteinstellung

OWN
  Stellt ein, ob man das Puppet nach dem Start vom Starter owned wird.\
  *Default*: YES

.. index:: pair: NEWOWNER-Option; Puppeteinstellung

.. _optionNewOwner:

NEWOWNER
  Legt fest, ob das Puppet von anderen Benutzern geownt werden darf. Wird automatisch auf ``NO`` gestellt, wenn ``CITYCHAT:YES`` gesetzt ist.\
  *Default*: YES

.. index:: pair: TWINDOW-Option; Puppeteinstellung

TWINDOW
  Das, was ein Puppet zu hören bekommt, wird nach ``/own PUPPET`` durch ``TWINDOW:YES`` in einem extra Fenster dargestellt. Ansonsten erscheint dies im normalen Chat.\
  *Default*: NO




Einstellungen für Puppetverhalten
=================================

.. index:: pair: CASESENSITIV-Option; Puppeteinstellung

CASESENSITIV
  Gibt an, ob das Puppet Groß-/Kleinschreibung bei den Events ``CHAT``, ``KEYWORD`` und ``MATCH`` unterscheidet.

  Die Einstellung ``CASESENSITIV:NO`` wird auch bei ``==, !=, IN`` (nur ``EVAL``!), ``STARTSWITH``, ``ENDSWITH`` und ``INLIST`` beachtet.

  *Default*: NO

.. index:: pair: OVERFLOW-Option; Puppeteinstellung

OVERFLOW
  Es wird festgelegt, was geschehen soll, wenn das Puppet zu viele Ressourcen (Speicher, ..) verbraucht:

  - ``ERROR`` = Es wird eine ERROR-Action aufgerufen (siehe Kommando :ref:`WHEN ERROR <befehlWHENERROR>`).
  - ``HARAKIRI`` = Das Puppet beendet sich.
  - ``IGNORE`` = Das Puppet ignoriert den Fehler.

  *Default*: HARAKIRI


.. index:: pair: ZEROINTVARS-Option; Puppeteinstellung

ZEROINTVARS
  Wenn ``YES`` angegeben wird, werden leere Variablen als "0" aufgefasst.

  *Default*: NO

  .. warning::

    Es gilt [undefined] >= 0 und [undefined] <= 0 aber nicht [undefined] == 0.

.. index:: pair: DEBUG-Option; Puppeteinstellung
.. index:: pair: DEBUG-Option; Debug

DEBUG
  Hiermit wird der DEBUG-Modus angeschaltet. Erlaubt ist jede Kombination der Einstellungen: ``ACTION``, ``EVENT``, ``VAR(IABLES)``, ``(SINGLE)STEP``, ``LOCAL``, ``SCOPE`` und ``TOOL``. Ausserdem ``ON``, ``OFF``, ``ALL`` und ``NONE``.\

  *Default*: "---"

  |more| Siehe PuppetTool und Kapitel :ref:`Debugging <debugging>`.\

<command>
  Aufruf der festgelegten Action als @-Befehl.\

  *Default*: "---"

  |more| Siehe Kapitel :ref:`@-Befehle <addBefehle>`.\



Einstellungen für Puppetdeklaration
===================================

.. index:: pair: CHARSET-Option; Puppeteinstellung

CHARSET
  Der Zeichensatz des Quelltextes wird festgelegt.

  - ``ISO-8859-1``
  - ``UTF-8``
  - ``Shift_JIS``

  *Default*: ISO-8859-1


.. |more| image:: /_static/more.png
          :align: middle
          :alt: more info
