
Befehle für Strings
===================


.. index:: pair: FIND-Befehl; Befehl

FIND-Befehl
-----------

::

  FIND <varname> <zeichen>

* Sucht in der Variablen mit Namen <varname> alle Zeichen <zeichen>.


*Liste der belegten Variablen*:

* ``INDEXLEN`` = Anzahl der Treffer
* ``INDEX*`` = Positionen der Treffer (\*=1,2,...)



.. index:: pair: REPLACE-Befehl; Befehl

REPLACE-Befehl
--------------

::

  REPLACE <varname> <von> <nach>

* Ersetzt in der Variablen mit Namen <varname> alle Vorkommen <von> durch <nach> (ohne Rücksicht auf Listenformate).



.. index:: pair: SUBSTR-Befehl; Befehl

SUBSTR-Befehl
-------------

::

  SUBSTR <varname> <indexVon> <indexBis>

* Ersetzt den String mit Namen <varname> durch eine Teilmenge seiner selbst.
* Die Teilmenge wird bestimmt durch die Zeichen zwischen den Indexpositionen <indexVon> und <indexBis>.
* Wird <indexBis> weggelassen, so werden die Zeichen bis zum Ende genommen.


.. tip::

  Für <indexVon> gleich <indexBis> empfiehlt sich der Ausdruck :ref:`CHAROF <ausdruckCHAROF>`.
